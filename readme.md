##关于探索DLNA的相关一些资料

##文件夹说明

```
1.DLNA zip文件里面包含一些DLNA的相关资料，多数是英文的
2.DLNA-Develop 是一个开源DLNA的客户端
3.DLNA_IOS_Platinum-master 是一个开源的DMS服务端
4.PlatinumKit是Platinum的开源实现

```

##关于安装minidlna

```
首先查看本机是否安装brew

如果安装了
brew search minidlna //查看是否可以安装minidlna

如果是
brew install minidlna //安装dlna

安装完毕后需要设置dlna服务

###设置目录
```
mkdir -p /Users/goswift/.config/minidlna
mkdir -p /Users/goswift/.config/minidlna/media
mkdir -p /Users/goswift/.config/minidlna/cache //goswift 是你的user目录

```

###添加一个软链接
```
cd /usr/local/bin

ln -s ../Cellar/minidlna/1.1.4_2/sbin/minidlnad minidlnad //这样可以通过which minidlnad找到
```
###创建配置文件
```
vi ~/.config/minidlna/minidlna.conf

friendly_name=Mac DLNA Server//名字可以自定义
media_dir=/Users/foo/.config/minidlna/media
db_dir=/Users/foo/.config/minidlna/cache
log_dir=/Users/foo/.config/minidlna
```

###如何启动dlna
```
在MAC上的启动 比较麻烦，研究了很久最后发现日志里面启动的方式

minidlnad -f ~/.config/minidlna/minidlna.conf -P ~/.config/minidlna/minidlna.pid

启动之后可以查看日志目录，启动端口默认8200
http://localhost:8200 
```
###停止杀掉进程
```
pkill minidlnad
```

###重新扫描本地目录
```
minidlnad -R -f ~/.config/minidlna/minidlna.conf -P ~/.config/minidlna/minidlna.pid

如果设置60秒 扫描一次目录
minidlnad -t 60 -f ~/.config/minidlna/minidlna.conf -P ~/.config/minidlna/minidlna.pid

```

##备注

```
1.DLNA 服务器端和客户端都可以运行，不过还不能两个联合使用
```
